# WSL2 / Docker Dev / Extension Example

This is a work in progress that aims to extend an existing Docker Dev image and build on top of it.

## Objectives (draft)

v1:

- [x] Add custom tool in wsl image
- [ ] Add custom service in wsl image (non-systemd)
  - Snippets to customise and start with are available in infra-setup/ folder
- [x] Export the image for user consumption

v2:

- [ ] hook into post-import-setup
- [ ] standard way to add services (systemd or not)
