#!make


.PHONY: kind-k8
kind-k8: setup download etc

# download and install kind
# setup profile to start it up automatically
# with 2 kubeconfig

.PHONY: .install-profile
.install-profile: .check .uninstall-profile .install-mount-paths
	@echo "Writing data-mount service start in ${PROFILE}"
	@echo ""
	@echo "# WSL data client (start)" >> ${PROFILE}
	@echo "if ! mountpoint -q bin/; then" >> ${PROFILE}
	@echo "  /mnt/c/Windows/system32/wsl.exe --cd / -d ${DATA_DISTRO} hostname 1>/dev/null 2>/dev/null" >> ${PROFILE}
	@echo "  sudo ${SHARE_BASE}/${SHARE_TARGET}/wsl-data-sharing/mount.start 1>/dev/null || echo Failed to mount data distro" >> ${PROFILE}
	@echo "fi" >> ${PROFILE}
	@echo "# WSL data client (end)" >> ${PROFILE}
	@echo ""
	@echo "Setting up sudoers for data-mount"
	@sudo rm -f /etc/sudoers.d/data-mount
	@echo "${USERNAME} ALL = NOPASSWD:${SHARE_BASE}/${SHARE_TARGET}/wsl-data-sharing/mount.start, ${SHARE_BASE}/${SHARE_TARGET}/wsl-data-sharing/mount.stop" | sudo tee /etc/sudoers.d/data-mount
	@echo ""
	@echo "Starting first mount"
	@sudo ${SHARE_BASE}/${SHARE_TARGET}/wsl-data-sharing/mount.start

.PHONY: .uninstall-profile
.uninstall-profile: .check
	@(test -n "${PROFILE_START}" && test -n "${PROFILE_END}") || echo "user ~/.profile mount service is not currently installed."
	@(test -n "${PROFILE_START}" && test -n "${PROFILE_END}") || \
		(test -z "${PROFILE_START}" && test -z "${PROFILE_END}") || \
		{ echo "${PROFILE} contains invalid start/stop blocks"; false; }
	@(test "${PROFILE_START}" = "" && test "${PROFILE_END}" = "") || \
		(sed -i "${PROFILE_START},${PROFILE_END}d" ${PROFILE} && echo "Block removed from profile")
