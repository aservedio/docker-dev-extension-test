H1COLOR=6
define h1
	$(call prn,1,${H1COLOR},$1)
endef

H2COLOR=2
define h2
	$(call prn,2,${H2COLOR},$1)
endef

define title
	$(call prn,$1,$2,$3)
endef

define answer
	printf "$1\n"
endef

define prn
	printf '%*s' $1 "" | tr ' ' '>' \
		&& printf " " \
		&& printf "$3" \
		&& (test "$(shell echo $3 | rev | cut -c1)" = ":" && printf " " || printf "\n")
endef
