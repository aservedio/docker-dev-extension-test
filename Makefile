#!make

MAKEFLAGS += --no-print-directory

TITLE = Docker Dev / Ubuntu 22.04 / Customised

PROJECT_TAG ?= v0.0.0
BUILD_REF ?= ${PROJECT_TAG}
BUILD_SOURCE ?= http://localhost

PACKAGE_DIR = packages
DOCKER_IMAGE ?= wsl-docker-dev-custom
PACKAGE_NAME ?= wsl-docker-dev-custom

WSL_USER ?= wsluser

all: info

.PHONY: info
info:
	$(info GeekStuff.dev / WSL2 / Docker Dev / Extension Example)
	$(info > make info)
	$(info > make build)
	$(info > make package)
	$(info > make shell)
	@:

# If in a devcontainer, dev-setup gets copied. Otherwise pipelines are
# expected to handle that before calling this.
.PHONY: build
build:
	docker build \
		-t ${DOCKER_IMAGE} \
		--build-arg "BUILD_REF=${BUILD_REF}" \
		--build-arg "BUILD_SOURCE=${BUILD_SOURCE}" \
		.
	@echo "Built image: [${DOCKER_IMAGE}]"

.PHONY: package
package: .pre-package
	@mkdir -p ${PACKAGE_DIR}
	docker export ${PACKAGE_NAME}-instance | gzip -c > ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@printf "Package size:\n  "
	@du -hs ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@$(MAKE .docker.cleanup)
	@! test -d /workspace || ( \
		echo "Example for local import:"; \
		echo "  wsl --import tst-docker-dev-local .\wsl-tst-docker-dev-local '\\\wsl$$\your-distro-name\home\your-user\wsl2-repo\packages\${PACKAGE_NAME}.tar.gz'"; \
		echo "  wsl -d tst-docker-dev-local post-import-setup"; \
		echo "For cleanup:"; \
		echo "  wsl --terminate tst-docker-dev-local"; \
		echo "  wsl --unregister tst-docker-dev-local"; \
		echo "  rm wsl-tst-docker-dev-local"; \
		echo "Devcontainer examples:"; \
		echo "  git clone https://gitlab.com/geekstuff.dev/playground/rust-cli.git"; \
	)

.PHONY: shell
shell:
	@echo "Launching a shell in an ephemeral docker instance."
	@echo "Things to try:"
	@echo "- post-import-setup"
	@echo "- sudo -u \$$WSL_USER -- bash -l"
	docker run --rm -it ${DOCKER_IMAGE}

.PHONY: shell-wsl-user
shell-wsl-user:
	docker run --rm -it ${DOCKER_IMAGE} sudo -u ${WSL_USER} -- bash -l

.PHONY: .pre-package
.pre-package: .docker.cleanup
	docker run --name ${PACKAGE_NAME}-instance ${DOCKER_IMAGE}

.PHONY: .docker.cleanup
.docker.cleanup:
	@docker stop ${PACKAGE_NAME}-instance 1>/dev/null 2>/dev/null || true
	@docker rm ${PACKAGE_NAME}-instance 1>/dev/null 2>/dev/null || true
