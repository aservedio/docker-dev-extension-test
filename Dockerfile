FROM registry.gitlab.com/geekstuff.dev/wsl2/docker-dev/ubuntu-22.04:v1.0

# Info on this repo
ARG BUILD_REF
ARG BUILD_SOURCE
ENV BUILD_REF=${BUILD_REF}
ENV BUILD_SOURCE=${BUILD_SOURCE}

COPY ./infra-setup/ /usr/lib/infra-setup/
WORKDIR /usr/lib/infra-setup/
RUN ["make", "-C", "1.tools"]
